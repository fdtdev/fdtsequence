# FDT Sequence

Graphic coroutine wrapper to design sequences in editor


## Usage

The package must be included using the manifest.json file in the project.
The file must be modified to include this dependencies:

```json
{
  "dependencies": {
	"com.fdt.sequence": "https://bitbucket.org/fdtdev/fdtsequence.git#2.0.0",
	"com.fdt.common": "https://bitbucket.org/fdtdev/fdtcommon.git#5.0.0",
	"com.fdt.gameevents": "https://bitbucket.org/fdtdev/fdtgameevents.git#2.0.0",

	...
  }
}

```

## License

MIT - see [LICENSE](https://bitbucket.org/fdtdev/fdtsequence/src/2.0.0/LICENSE.md)