﻿using com.FDT.Common.Editor;
using UnityEngine;
using UnityEditor;
using com.FDT.Common.RotorzReorderableList;
using com.FDT.GameEvents;

namespace com.FDT.Sequence.Editor
{ 
    [CustomEditor(typeof(Sequence))]
    public class SequenceEditor : UnityEditor.Editor {

        private SerializedProperty _itemsProperty;
        private SequenceAdaptor _adaptor;
        private ReorderableListControl _sequenceItemList;
        private void OnEnable()
        {
            _itemsProperty = serializedObject.FindProperty("_items");
            _adaptor = new SequenceAdaptor(_itemsProperty, 0);
            _sequenceItemList = new ReorderableListControl();
            _sequenceItemList.ItemInserted += HandleItemInserted;
        }
        private void HandleItemInserted(object sender, ItemInsertedEventArgs args)
        {
            SerializedProperty p = _itemsProperty.GetArrayElementAtIndex(args.ItemIndex);
            p.FindPropertyRelative("_active").boolValue = true;
            p.FindPropertyRelative("_autocomplete").boolValue = true;
            p.FindPropertyRelative("_folded").boolValue = true;
            
        }
        private void OnDisable()
        {
            if (_sequenceItemList != null)
            {
                _sequenceItemList.ItemInserted -= HandleItemInserted;
            }
        }
        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.PropertyField(serializedObject.FindProperty("_autoRun"), true);
            EditorGUILayout.Space();
            ReorderableListGUI.Title("Items");
            _sequenceItemList.Draw(_adaptor);
            EditorGUILayout.Space();
            ReorderableListGUI.Title("On Finish Item List Event");
            EditorGUILayout.PropertyField(serializedObject.FindProperty("_endEvt"), true);
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            serializedObject.ApplyModifiedProperties();
        }
    }


    [CustomPropertyDrawer(typeof(SequenceItem))]
    public class SequenceItemDrawer : UnityEditor.PropertyDrawer
    {
        private static float minWidth = 250;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            Color oldBackColor = GUI.backgroundColor;
            Color blackColor = Color.black;
            blackColor.r = 0.5f;
            blackColor.g = 0.5f;
            blackColor.b = 0.5f;
            blackColor.a = 1f;
            Color backgroundDefaultColor = EditorGUIUtility.isProSkin ? new Color32(56, 56, 56, 255) : new Color32(222, 222, 222, 255);
            SerializedProperty autoProp = property.FindPropertyRelative("_autocomplete");
            SerializedProperty eventProp = property.FindPropertyRelative("_event");
            SerializedProperty colorProp = property.FindPropertyRelative("_color");
            SerializedProperty actProp = property.FindPropertyRelative("_active");

            bool mini = position.width < minWidth;
            EditorGUI.BeginProperty(position, label, property);


            position = new Rect(position.x + 7, position.y, position.width - 7, position.height);
            SerializedProperty foldedProp = property.FindPropertyRelative("_folded");
            foldedProp.boolValue = EditorGUI.Foldout(new Rect(position.x, position.y, 7,17), foldedProp.boolValue, GUIContent.none);

            if (actProp.boolValue)
            {
                GUI.backgroundColor = colorProp.colorValue;
            }

            if (foldedProp.boolValue)
            {
                EditorGUI.BeginDisabledGroup(!actProp.boolValue);
                GUI.Box(position, GUIContent.none);     

                position = new Rect(position.x + 2, position.y + 2, position.width - 4, position.height - 4);
                Rect descRect = new Rect(position.x, position.y, position.width, 40);
                float descWidth = mini ? 40 : 80;
                GUI.BeginGroup(descRect, GUIContent.none, "Box");
                EditorGUI.LabelField(new Rect(0, 2, descWidth, 16), mini?"Desc:":"Description:");
                EditorGUI.PropertyField(new Rect(descWidth, 2, descRect.width*0.6f- descWidth, 16), property.FindPropertyRelative("_description"), GUIContent.none);
                EditorGUI.LabelField(new Rect(descRect.width*0.6f, 2, 40, 16), "Color:");
                EditorGUI.BeginChangeCheck();
                EditorGUI.PropertyField(new Rect((descRect.width * 0.6f) + 40, 2, (descRect.width * 0.4f) - 44, 16), colorProp, GUIContent.none);
                if (EditorGUI.EndChangeCheck())
                {
                    Color c = colorProp.colorValue;
                    c.a = Mathf.Min(c.a, 0.5f);
                    colorProp.colorValue = c;
                }

                // buttons
                float activeWidth = 45;
                float autoWidth = 88;
                float dist = (descRect.width - activeWidth - autoWidth) * 0.25f;
                Rect btnActiveRect = new Rect(dist, 20, activeWidth, 16);
                Rect btnAutoCompleteRect = new Rect((dist*3)+ activeWidth, 20, autoWidth, 16);

                EditorGUI.EndDisabledGroup();
                actProp.boolValue = GUI.Toggle(btnActiveRect, actProp.boolValue, "Active", EditorStyles.miniButton);                
                EditorGUI.BeginDisabledGroup(!actProp.boolValue);

                autoProp.boolValue = GUI.Toggle(btnAutoCompleteRect, autoProp.boolValue, "Autocomplete", EditorStyles.miniButton);
                
                GUI.EndGroup();

                Rect btnsRect = new Rect(position.x, position.y+20, position.width, 20);
                
                float h = eventProp.GetHeight();

                Rect evtRect = new Rect(position.x, position.y + 40 + 2, position.width, h);

                EditorGUI.PropertyField(evtRect, eventProp);

                Rect waitsRect = new Rect(position.x, position.y + 40 + h + 4, position.width, 22);

                GUI.BeginGroup(waitsRect, GUIContent.none, "Box");
                float beforeWidth = mini ? 50 : 80;
                float afterWidth = mini ? 35 : 70;
                EditorGUI.LabelField(new Rect(2,3, beforeWidth, 16), mini?"Before:":"Wait Before:");
                EditorGUI.PropertyField(new Rect(beforeWidth, 3, (waitsRect.width * 0.5f) - beforeWidth - 3, 16), property.FindPropertyRelative("_waitBefore"), GUIContent.none);
                EditorGUI.LabelField(new Rect(waitsRect.width * 0.5f, 3, afterWidth, 16), mini?"After:":"Wait After:");
                EditorGUI.PropertyField(new Rect((waitsRect.width * 0.5f)+ afterWidth, 3, (waitsRect.width * 0.5f) - afterWidth - 3, 16), property.FindPropertyRelative("_waitAfter"), GUIContent.none);
                GUI.EndGroup();
                EditorGUI.EndDisabledGroup();
            }
            else
            {
                EditorGUI.BeginDisabledGroup(!actProp.boolValue);
                GUI.BeginGroup(position, GUIContent.none, EditorStyles.helpBox);
                Color c = colorProp.colorValue;
                c.a = Mathf.Min(0.35f, c.a);
                GUI.backgroundColor = backgroundDefaultColor;
                GUI.Box(new Rect(1, 1, position.width - 2, position.height - 2), GUIContent.none);
                GUI.backgroundColor = c;
                GUI.Box(new Rect(1, 1, position.width - 2, position.height - 2), GUIContent.none);
                GUI.backgroundColor = oldBackColor;
                EditorGUI.LabelField(new Rect(2, 2, position.width - 56, 16), property.FindPropertyRelative("_description").stringValue);
                GUI.Toggle(new Rect(position.width-53, 2, 25, 16), autoProp.boolValue, "AC", EditorStyles.miniButton);
                bool tmp = true;
                MultiEvent.MultiEventType t = (MultiEvent.MultiEventType)eventProp.FindPropertyRelative("_type").enumValueIndex;
                GUI.Toggle(new Rect(position.width - 27, 2, 25, 16), tmp, t == MultiEvent.MultiEventType.GAME_EVENT?"GE":"UE", EditorStyles.miniButton);
                GUI.EndGroup();
                EditorGUI.EndDisabledGroup();
            }
            GUI.backgroundColor = oldBackColor;
            EditorGUI.EndProperty();
        }
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            SerializedProperty foldedProp = property.FindPropertyRelative("_folded");
            if (foldedProp.boolValue)
            {
                SerializedProperty eventProp = property.FindPropertyRelative("_event");
                float h = eventProp.GetHeight();
                return 79 + h;
            }
            else
            {
                return 20;
            }
        }
    }
}