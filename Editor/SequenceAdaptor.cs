﻿using UnityEngine;
using com.FDT.Common.RotorzReorderableList;
using UnityEditor;

namespace com.FDT.Sequence.Editor
{
    public class SequenceAdaptor : SerializedPropertyAdaptor
    {
        public SequenceAdaptor(SerializedProperty arrayProperty) : base(arrayProperty)
        {
        }

        public SequenceAdaptor(SerializedProperty arrayProperty, float fixedItemHeight) : base(arrayProperty, fixedItemHeight)
        {
        }
        
        public override void DrawItemBackground(Rect position, int index)
        {
            GUIStyle BackStyle = new GUIStyle();
            BackStyle.normal.background = EditorGUIUtility.whiteTexture;
            BackStyle.stretchWidth = true;
            Color oldC = GUI.color;
            bool drawBack = false;
            var s = this[index].FindPropertyRelative("_state").enumValueIndex;
            if ((SequenceItem.SequenceState)s == SequenceItem.SequenceState.OFF || (SequenceItem.SequenceState)s == SequenceItem.SequenceState.FINISHED)
            {

            }
            else if ((SequenceItem.SequenceState)s == SequenceItem.SequenceState.WAITING_BEFORE)
            {
                GUI.color = Color.yellow;
                drawBack = true;
            }
            else if ((SequenceItem.SequenceState)s == SequenceItem.SequenceState.WAITING_AFTER)
            {
                GUI.color = Color.magenta;
                drawBack = true;
            }
            else if ((SequenceItem.SequenceState)s == SequenceItem.SequenceState.EXECUTING)
            {
                GUI.color = Color.cyan;
                drawBack = true;
            }
            if (drawBack)
            {
                BackStyle.Draw(position, false, false, false, false);
                drawBack = false;
            }
            GUI.color = oldC;

            base.DrawItemBackground(position, index);
        }
    }
}