﻿using UnityEngine;
using com.FDT.GameEvents;

namespace com.FDT.Sequence
{
    [System.Serializable]
    public class SequenceItem
    {
        public enum SequenceState
        {
            OFF = 0, WAITING_BEFORE = 1, EXECUTING = 2, WAITING_AFTER = 3, FINISHED = 4
        }
#if UNITY_EDITOR
        [SerializeField] protected bool _folded;
        [SerializeField] protected Color _color;
#endif
        [SerializeField] protected SequenceState _state;
        public SequenceState State { get { return _state; } set { _state = value; } }

        [SerializeField] protected string _description;

        [SerializeField] protected bool _active = true;
        public bool Active { get { return _active; } }

        [SerializeField] protected bool _autocomplete = true;
        public bool Autocomplete { get { return _autocomplete; } }

        [SerializeField] protected float _waitBefore;
        public float WaitBefore { get { return _waitBefore; } }

        [SerializeField] protected float _waitAfter;
        public float WaitAfter { get { return _waitAfter; } }

        [SerializeField] protected MultiEvent _event;
        public MultiEvent Event { get { return _event; } }
    }
}