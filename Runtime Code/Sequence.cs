using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using com.FDT.GameEvents;

namespace com.FDT.Sequence
{
    public class Sequence : MonoBehaviour {

        [SerializeField] protected List<SequenceItem> _items = new List<SequenceItem>();
        public List<SequenceItem> items {  get { return _items;  } }

        protected bool currentItemComplete = false;
        protected int _currentItemIdx = 0;

        protected SequenceItem CurrentItem { get { return _items[_currentItemIdx]; } }

        [SerializeField] protected MultiEvent _endEvt;

        [SerializeField] protected bool _autoRun = false;
        
        protected virtual bool AutoRun
        {
            get { return _autoRun; }
        }
        public void Awake()
        {
            if (AutoRun)
                Run();
        }
        public void Run()
        {
            _currentItemIdx = -1;
            ExecuteNextItem();
        }
        protected void ExecuteCurrentItem()
        {
            if (CurrentItem.WaitBefore > 0)
            {
                StartCoroutine(DoWaitBefore());
            }
            else
            { 
                DoExecute();
            }
        }
        protected IEnumerator DoWaitBefore()
        {
            CurrentItem.State = SequenceItem.SequenceState.WAITING_BEFORE;
            float endTime = Time.time + CurrentItem.WaitBefore;
            do { yield return null; } while (Time.time < endTime);
            DoExecute();
        }
        protected void DoExecute()
        {
            CurrentItem.State = SequenceItem.SequenceState.EXECUTING;
            CurrentItem.Event.Execute();
            if (CurrentItem.Autocomplete || currentItemComplete)
            {
                currentItemComplete = false;
                CheckWaitAfter();
            }
            else
            {
                StartCoroutine(DoWaitForCompletion());
            }
        }
        protected IEnumerator DoWaitForCompletion()
        {
            do
            {
                yield return null;
            }
            while (!currentItemComplete);
            CheckWaitAfter();
        }
        protected void CheckWaitAfter()
        {
            if (CurrentItem.WaitAfter > 0)
            {
                StartCoroutine(DoWaitAfter());
            }
            else
            {
                ExecuteNextItem();
            }
        }
        protected IEnumerator DoWaitAfter()
        {
            CurrentItem.State = SequenceItem.SequenceState.WAITING_AFTER;
            float endTime = Time.time + CurrentItem.WaitAfter;
            do { yield return null; } while (Time.time < endTime);
            ExecuteNextItem();
        }
        public void ItemComplete()
        {
            if (!CurrentItem.Autocomplete && !currentItemComplete)
            {
                currentItemComplete = true;
            }
            else if (!CurrentItem.Autocomplete && currentItemComplete)
            {
                Debug.LogWarning("called autocomplete more than one time for the same item ?");
            }
            else if (CurrentItem.Autocomplete)
            {
                Debug.LogWarning("Called ItemComplete for an item not waiting for ItemComplete.");
            }
        }
        protected void ExecuteNextItem()
        {
            currentItemComplete = false;
            if (_currentItemIdx != -1)
                CurrentItem.State = SequenceItem.SequenceState.FINISHED;

            if (_currentItemIdx < _items.Count-1)
            {
                bool found = false;
                while (_currentItemIdx < _items.Count - 1 && !found)
                {
                    _currentItemIdx++;
                    if (CurrentItem.Active)
                        found = true;
                }
                if (found)
                    ExecuteCurrentItem();
                else
                    EndSequence();
            }
            else
            {
                EndSequence();
            }
        }
        protected void EndSequence()
        {
            _endEvt.Execute();
        }
    }
}